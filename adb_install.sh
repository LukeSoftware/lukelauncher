#! /usr/bin/env bash

_SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

_APP_APK="$_SCRIPT_DIR/platforms/android/app/build/outputs/apk/debug/app-debug.apk"
_APP_NAME="luke.launcher"

if [ -f "$_APP_APK" ]; then
    adb shell am force-stop "$_APP_NAME"; adb install -r "$_APP_APK"
else
    echo "File not found: $_APP_APK"
    exit 1
fi