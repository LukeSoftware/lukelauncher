#! /usr/bin/env bash

_SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

_BUILD_DIR="$_SCRIPT_DIR/.build"

_APP_NAME="luke.launcher"
_APP_BACKUP_DATA="$_BUILD_DIR/$_APP_NAME.data.ab"

if [ -f "$_APP_BACKUP_DATA" ]; then
    adb shell am force-stop "$_APP_NAME"; adb restore "$_APP_BACKUP_DATA"
else
    echo "File not found: $_APP_BACKUP_DATA"
    exit 1
fi