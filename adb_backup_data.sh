#! /usr/bin/env bash

_SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

_BUILD_DIR="$_SCRIPT_DIR/.build"

_APP_NAME="luke.launcher"
_APP_BACKUP_DATA="$_BUILD_DIR/$_APP_NAME.data.ab"

adb backup -f "$_APP_BACKUP_DATA" "$_APP_NAME"