var settings_text_clock_app = "";
var settings_text_date_app = "";
var search_place_holder = "";
var app_order_li = "";
var app_order_us_li = "";
var app_move_li = "";
var hide_app_li = "";
var settings_li = "";
var rename_app = "";
var start_app = "";
var add_app_to_homescreen_text = "";
var rm_app_from_homescreen_text = "";
var hide_app_text = "";
var show_app_text = "";
var app_unin = "";
var settings_header = "";
var settings_homescreen_icons_text = "";
var settings_appdrawer_text = "";
var settings_icon_type_text = "";
var settings_icon_type_homescreen_text = "";
var settings_homescreen_day_time_text = "";
var settings_text_color_text = "";
var settings_text_colorhome_text = "";
var settings_read_text = "";
var modal_header_text_color_text = "";
var modal_header_homescreentext_color_text = "";
var ok_t_color_text = "";
var ok_th_color_text = "";
var modal_header_apd_text = "";
var modal_header_ormode_text = "";
var modal_ormode_l_text = "";
var modal_ormode_r_text = "";
var modal_header_homescreen_icon_text = " ";
var modal_homescreen_icon_r_text = "";
var modal_homescreen_icon_k_text = "";
var modal_header_clock_pos_text = "";
var btn_applay_clock_pos_text = "";
var modal_header_clock_settings_text = "";
var modal_clock_show_text = "";
var modal_sec_show_text = "";
var modal_hour_show_text = "";
var modal_date_show_text = "";
var clock_showing_true = "";
var clock_showing_false = "";
var bg_color_text = "";
var bg_img_text = "";
var modal_header_lang_text = "";
var modal_lang_ger_text = "";
var modal_lang_eng_text = "";
var modal_lang_spa_text = "";
var modal_lang_cz_text = "";
var modal_lang_it_text = "";
var modal_lang_fr_text = "";
var current_lang = "";
var modal_header_lang_text2 = "";
var settings_hs_sp_text = "";
var settings_hs_sp_text_header = "";
var settings_error_string = "";
var move_apps_text = "";
var result_string = "";
var removehomescreen_string = "";
var app_list_updated = "";
var settings_update_app_list_text = "";
var settings_navbar_text = "";
var hidden_apps_text = "";
var font_size_text = "";
var font_cat_text = "";
var font_app_text = "";
var font_homescreen_text = "";
var text_navbar_complete_transparent = "";
var header_navbar_text = "";
var text_navbar_none_transparent = "";
var text_pad_top_input = "";
var text_pad_bottom_input = "";
var settings_homescreeen_notification_text = "";
var text_settings_app_drawer = "";
var text_app_drawer_hori = "";
var text_app_drawer_verti = "";
var text_homescreensettings_drawer = "";
var text_homescreen_zeilen = "";
var text_homescreen_spalten = "";
var settings_statusbar_text = "";
var cat_settings_text = "";
var reset_usage_text = "";
var info_text1 = "";
var info_text2 = "";
var info_text3 = "";
var cat_icon_size_text = "";
var header_icon_source_text = "";
var btn_orginal_icon_text = "";
var btn_gallery_icon_text = "";
var btn_symbol_icon_text = "";
var header_select_icon_pack = "";
var header_select_icon_from_iconpack_text = "";
var header_iconpack_choose_text = "";
var default_string = "";
var progress_info_string = "";
var export_string = "";
var import_string = "";
var add_new_catbtn_string = "";
var resortapplist_string = "";
var ask_resport_string = "";
var resort_btn_string = "";
var abort_btn_string = "";
var resort_error_string = "";
var export_msg_string = "";
var aling_left_string = "";
var aling_right_string = "";
var swipe_header_string = "";
var settings_appdrawericonsize_string = "";
var sort_apps_new_string = "";
var modal_border_show_string = "";
var cat_default_text = "";
var extra_text_string = "";
var appdrawer_align_string = "";
var cat_custom_text = "";
var del_cat_text = "";
var notifi_allow_string = "";
var notifi_allow_ok_string = "";
var no_iconpack_installed_string = "";
var overwrite_import_string = "";
var import_btn_string = "";
var btn_bottomzero_string = "";
var msg_success = "";
var modal_alarm_clock_text = "";
var next_day_string = "";
var warn_sort_cat_string = "";
var font_string = "";
var font_select_string = "";
var orient_text = "";
var orient_auto = "";
var orient_horizontal = "";
var orient_vertical = "";
var expand_notification_text = "";
var widget_text = "";
var widget_background_text = "";
var widget_scroll_text = "";
var widget_activ = "";
var widget_enabled = "";
var widget_disabled = "";
var widget_up = "";
var widget_down = "";
var widget_add = "";
var widget_add_button = "";
var widget_info = "";
var all_widget_headers_text = "";
var widget_inform_textr = "";
var widget_inform_textl = "";
var help_1 = "";
var help_2 = "";
var help_3 = "";
var animation_text = "";
var export_error = "";
var import_error = "";
var more_icons = "";
var header_part_display_text = "";
var header_part_behave_text = "";
var header_part_text_text = "";
var header_part_icons_text = "";

//All element strings are changed in order to apply the current language
function applay_lang()
{
    lang = lang.replace("de", "ger");
    var l_cat_array = [];

    if(lang == "ger")
    {
        //German
        info_text1 = "Erstellt von Lukas Sökefeld";
        info_text2 = "Version:";
        info_text3 = "Mehr Information:";
        text_pad_top_input = "Abstand oben";
        font_size_text = "Schriftgröße";
        hidden_apps_text = "Versteckte Apps";
        settings_navbar_text = "Transparente Navigationsleiste";
        settings_statusbar_text = "Statusbar anzeigen";
        settings_update_app_list_text = "App-liste aktualisieren";
        app_list_updated = "App-liste wurde aktualisiert!";
        removehomescreen_string = "Entfernen von Startbildschirm";
        result_string = "Ergebnisse";
        move_apps_text = "Apps verschieben";
        settings_error_string = "Fehler! Einstellungen!";
        settings_text_date_app = "Datum App";
        settings_text_clock_app = "Uhr App";
        search_place_holder = "Suche...";
        app_order_li = "Sortieren A-Z";
        app_order_us_li = "Sortieren Nutzung";
        app_move_li = "Apps verschieben";
        hide_app_li = "Versteckte Apps";
        settings_li = "Einstellungen";
        rename_app = "Umbenennen";
        start_app = "Starten";
        add_app_to_homescreen_text = "Hinzufügen - Startbildschirm";
        rm_app_from_homescreen_text = "Entfernen - Startbildschirm";
        hide_app_text = "Verstecken";
        show_app_text = "Anzeigen";
        app_unin = "Deinstallieren";
        settings_header = "Einstellungen";
        settings_homescreen_icons_text = "Hintergrung Icons";
        settings_appdrawer_text = "App-Übersicht Art";
        settings_icon_type_text = "App Icons Schwarz/Weiß";
        settings_icon_type_homescreen_text = "App Icons Schwarz/Weiß";
        settings_homescreen_day_time_text = "Zeit/Datum";
        settings_text_color_text = "Text Farbe";
        settings_text_colorhome_text = "Text Farbe";
        settings_read_text = "Lesbarkeit";
        modal_header_text_color_text = "Text Farbe";
        modal_header_homescreentext_color_text = "Text Farbe";
        ok_t_color_text = "Anwenden";
        ok_th_color_text = "Anwenden";
        modal_header_apd_text = "App-Übersicht:";
        modal_header_ormode_text = "Kategorien Position";
        modal_ormode_l_text = "links";
        modal_ormode_r_text = "rechts";
        modal_header_homescreen_icon_text = "Hintergrund Icons";
        modal_homescreen_icon_r_text = "Rechteckig";
        modal_homescreen_icon_k_text = "Keine";
        modal_header_clock_pos_text = "Position der Uhr";
        btn_applay_clock_pos_text = "Anwenden";
        modal_header_clock_settings_text = "Uhr Einstellungen";
        modal_clock_show_text = "Darstellen";
        modal_sec_show_text = "Sekunden anzeigen";
        modal_hour_show_text = "Stunden fett anzeigen";
        modal_date_show_text = "Datum anzeigen";
        clock_showing_true = "Wird angezeigt";
        clock_showing_false = "Wird nicht angezeigt";
        bg_color_text = "Farbe";
        bg_img_text = "Bild";
        modal_header_lang_text = "Sprache";
        modal_header_lang_text2 = "Sprache auswählen";
        modal_lang_ger_text = "Deutsch";
        modal_lang_eng_text = "Englisch";
        modal_lang_spa_text = "Spanisch";
        modal_lang_cz_text = "Tschechisch";
        modal_lang_it_text = "Italienisch";
        modal_lang_fr_text = "Französisch";
        current_lang = "Deutsch";
        settings_hs_sp_text = "Startbildschirm Spalten";
        settings_hs_sp_text_header = "Startbildschirm Spalten auswählen";
        font_cat_text = "Kategorien Titel";
        font_app_text = "App Namen";
        font_homescreen_text = "Schriftgröße Uhr";
        header_navbar_text = "Navigationsleiste Transparenz:";
        text_navbar_complete_transparent = "Komplett";
        text_navbar_none_transparent = "Keine";
        settings_homescreeen_notification_text = "Benachrichtigungen";
        text_pad_bottom_input = "Abstand unten";
        text_settings_app_drawer = "App-Übersicht";
        text_app_drawer_hori = "Spalten (Wenn Horizontal)";
        text_app_drawer_verti = "Spalten (Wenn Vertikal)";
        text_homescreensettings_drawer = "Startbildschirm";
        text_homescreen_zeilen = "Zeilen";
        text_homescreen_spalten = "Spalten";
        cat_settings_text = "App Kategorien";
        reset_usage_text = "App Nutzung zurücksetzen";
        cat_default_text = "Standard";
        cat_custom_text = "Angepasst";
        cat_icon_size_text = "Icon Größe";
        header_icon_source_text = "Icon Quelle";
        btn_orginal_icon_text = "Orginal";
        btn_gallery_icon_text = "Aus Gallerie";
        btn_symbol_icon_text = "Aus Symbol Paket";
        header_select_icon_pack = "Paket wählen";
        header_select_icon_from_iconpack_text = "Icon wählen";
        header_iconpack_choose_text = "Icon-pack wählen";
        default_string = "Standard";
        progress_info_string = "Wende an...";
        extra_text_string = "Weiteres";
        export_string = "Einstellungen exportieren";
        import_string = "Einstellungen importieren";
        add_new_catbtn_string = "Neue Kategorie hinzufügen";
        resortapplist_string = "Appliste neu sortieren";
        ask_resport_string = "Achtung! die bestehende App-Sortierung wird verworfen!";
        resort_btn_string = "Sortieren";
        abort_btn_string = "Abbrechen";
        resort_error_string = "Sortieren funktioniert nur bei Standard App Kategorien";
        export_msg_string = "Einstellungen sind auf Standardspeicher exportiert";
        appdrawer_align_string = "Ausrichtung";
        aling_left_string = "Links";
        aling_right_string = "Rechts";
        swipe_header_string = "Geste";
        settings_appdrawericonsize_string = "Icon Größe";
        sort_apps_new_string = "Sortiere Apps neu";
        modal_border_show_string = "Hintergrund";
        del_cat_text = "Löschen";
        notifi_allow_string = "LukeLauncher erlauben, um Benachrichtigungen zu erhalten";
        notifi_allow_ok_string = "Ok";
        no_iconpack_installed_string = "Kein Icon pack installiert!";
        overwrite_import_string = "Achtung die bestehenden Einstellungen werden überschrieben!";
        import_btn_string = "Importieren";
        btn_bottomzero_string = "Abstand unten auf 0, wenn horrizontal";
        msg_success = "Erfolgreich!";
        modal_alarm_clock_text = "Alarm anzeigen";
        next_day_string = "Morgen";
        warn_sort_cat_string = "Apps werden nun nicht automatisch sortiert!";
        font_string = "Schriftart";
        font_select_string = "Schriftart wählen:";
        orient_text = "Ausrichtung";
        orient_auto = "Automatisch";
        orient_horizontal = "Horizontal";
        orient_vertical = "Vertikal";
        expand_notification_text = "Wischen um Statusbar auszuklappen";
        widget_text = "Widgets";
        widget_background_text = "Hintergrund wie App-Übersicht";
        widget_scroll_text = "Scrollen im Widget";
        widget_enabled = "Aktiviert";
        widget_disabled = "Deaktiviert";
        info_text = "Info";
        widget_up = "Hoch";
        widget_down = "Runter";
        widget_add = "Hinzufügen";
        widget_add_button = "Neues Widget hinzufügen";
        widget_info = "Lang drücken um Widget Höhe zu bearbeiten";
        all_widget_headers_text = "Aktive Widgets:";
        enable_widget_header_text = "Widgets aktivieren";
        widget_inform_textl = "Für Widget zugriff auf Homescreen nach links wischen.";
        widget_inform_textr = "Für Widget zugriff auf Homescreen nach rechts wischen.";
        help_1 = "App lang drücken zum anpassen.\nMenü zum verschieben in die Kategorien.";
        help_2 = "Nach rechts wischen um auf den Homescreen zu gelangen.";
        help_3 = "Nach rechts wischen um auf Widgets zuzugreifen.\nKann in den Einstellungen deaktiviert werden.";
        animation_text = "Animationen";
        export_error = "Konnte export nicht erstellen!";
        import_error = "Konnte nicht importieren!";
		more_icons = "Weitere";
		header_part_display_text = "Darstellung";
		header_part_behave_text = "Verhalten";
		header_part_text_text = "Schrift";
		header_part_icons_text = "Icons";


        if(default_cats == 1)
        {
            l_cat_array = [];
            l_cat_array = [
                ['icons/1.svg', "Nachrichten"],
                ['icons/2.svg', "Internet"],
                ['icons/3.svg', "Medien"],
                ['icons/4.svg', "Spiele"],
                ['icons/5.svg', "Werkzeuge"],
                ['icons/6.svg', "Einstellungen"]
            ];
        }
    }
    else if(lang == "spa")
    {
        //Spanisch from: Carlos Luna - caralu74@pm.me
        info_text1 = "Creado por Lukas Sökefeld";
        info_text2 = "Versión:";
        info_text3 = "Información adicional:";
        text_pad_top_input = "Espacio desde la barra de notificación";
        font_size_text = "Tamaño de la letra";
        hidden_apps_text = "Aplicaciones ocultas";
        settings_navbar_text = "Barra de navegación transparente";
        settings_statusbar_text = "Mostrar barra de estado";
        settings_update_app_list_text = "Actualizar lista de aplicaciones";
        app_list_updated = "Lista de aplicaciones actualizada!";
        removehomescreen_string = "Quitar de la pantalla de inicio";
        result_string = "Resultados";
        move_apps_text = "Mover aplicaciones";
        settings_error_string = "¡Error! - revise las opciones";
        settings_text_date_app = "Seleccionar aplicación de fecha";
        settings_text_clock_app = "Seleccionar aplicación de reloj";
        search_place_holder = "Buscar...";
        app_order_li = "Ordenar A-Z";
        app_order_us_li = "Ordenar uso";
        app_move_li = "Mover aplicaciones";
        hide_app_li = "Mostrar aplicaciones ocultas";
        settings_li = "Opciones";
        rename_app = "Renombrar";
        start_app = "Iniciar";
        add_app_to_homescreen_text = "Agregar a la pantalla de inicio";
        rm_app_from_homescreen_text = "Quitar de la pantalla de inicio";
        hide_app_text = "Ocultar";
        show_app_text = "Mostrar";
        app_unin = "Desinstalar";
        settings_header = "Opciones";
        settings_homescreen_icons_text = "Iconos de fondo";
        settings_appdrawer_text = "Tipo de caja-de-apps";
        settings_icon_type_text = "Íconos blanco/negro caja-de-apps";
        settings_icon_type_homescreen_text = "Iconos de la aplicación en blanco/negro";
        settings_homescreen_day_time_text = "Hora/fecha";
        settings_text_color_text = "Color del texto";
        settings_text_colorhome_text = "Color del texto";
        settings_read_text = "Legibilidad";
        modal_header_text_color_text = "Color del texto";
        modal_header_homescreentext_color_text = "Color de texto";
        ok_t_color_text = "Aplicar";
        ok_th_color_text = "Aplicar";
        modal_header_apd_text = "Caja-de-apps:";
        modal_header_ormode_text = "Escoja orientación";
        modal_ormode_l_text = "Izquierda";
        modal_ormode_r_text = "Derecha";
        modal_header_homescreen_icon_text = "Iconos de fondo";
        modal_homescreen_icon_r_text = "Cubo";
        modal_homescreen_icon_k_text = "Ninguno";
        modal_header_clock_pos_text = "Posición del reloj";
        btn_applay_clock_pos_text = "Aplicar";
        modal_header_clock_settings_text = "Opciones del reloj";
        modal_clock_show_text = "Mostrar";
        modal_sec_show_text = "Mostrar segundos";
        modal_hour_show_text = "Mostrar hora en negrilla";
        modal_date_show_text = "Mostrar fecha";
        clock_showing_true = "Se muestra";
        clock_showing_false = "No se muestra";
        bg_color_text = "Color";
        bg_img_text = "Imagen";
        modal_header_lang_text = "Idioma";
        modal_header_lang_text2 = "Seleccione el idioma";
        modal_lang_ger_text = "Alemán";
        modal_lang_eng_text = "Inglés";
        modal_lang_spa_text = "Español";
        modal_lang_cz_text = "Checo";
        modal_lang_it_text = "Italiano";
        modal_lang_fr_text = "Francés";
        current_lang = "Español";
        settings_hs_sp_text = "Columnas pantalla de inicio";
        settings_hs_sp_text_header = "Seleccionar columnas en pantalla de inicio";
        font_cat_text = "Título de categoría";
        font_app_text = "Nombre de apps";
        font_homescreen_text = "Reloj de tamaño de letra";
        header_navbar_text = "Transparencia de la barra de navegación:";
        text_navbar_complete_transparent = "Finalizado";
        text_navbar_none_transparent = "Ninguna";
        settings_homescreeen_notification_text = "Notificaciones";
        text_pad_bottom_input = "Espacio inferior de relleno";
        text_settings_app_drawer = "Opciones de la caja-de-apps";
        text_app_drawer_hori = "Columnas (cuando horizontal)";
        text_app_drawer_verti = "Columnas (cuando vertical)";
        text_homescreensettings_drawer = "Opciones de la pantalla de inicio";
        text_homescreen_zeilen = "Filas";
        text_homescreen_spalten = "Columnas";
        cat_settings_text = "Categoría de apps";
        reset_usage_text = "Restablecer App uso";
        cat_default_text = "Predeterminado";
        cat_custom_text = "Personalizar";
        cat_icon_size_text = "Tamaño de ícono";
        header_icon_source_text = "Fuente de íconos";
        btn_orginal_icon_text = "Original";
        btn_gallery_icon_text = "De galería";
        btn_symbol_icon_text = "De paquete de íconos";
        header_select_icon_pack = "Seleccione el paquete";
        header_select_icon_from_iconpack_text = "Seleccione el ícono";
        header_iconpack_choose_text = "Seleccione el paquete de íconos";
        default_string = "Predeterminado";
        progress_info_string = "Aplicar...";
        extra_text_string = "Adicional";
        export_string = "Exportar ajustes";
        import_string = "Importar ajustes";
        add_new_catbtn_string = "Agregar una nueva categoría";
        resortapplist_string = "Reorganizar lista de aplicaciones";
        ask_resport_string = "¡Atención! La reorganización de apps se ha descartado";
        resort_btn_string = "Ordenar";
        abort_btn_string = "Abandonar";
        resort_error_string = "Reorganizar solo funciona con las categorias predeterminadas";
        export_msg_string = "Los ajustes son exportados al almacenamiento estándar";
        appdrawer_align_string = "Alineación";
        aling_left_string = "Izquierda";
        aling_right_string = "Derecha";
        swipe_header_string = "Dirección de deslizamiento";
        settings_appdrawericonsize_string = "Tamaño de icono";
        sort_apps_new_string = "Las apps serán reorganizadas nuevamente por categorías";
        modal_border_show_string = "Fondo";
        del_cat_text = "Borrar";
        notifi_allow_string = "Permitir a LukeLauncher recibir notificaciones";
        notifi_allow_ok_string = "Ok";
        no_iconpack_installed_string = "No hay paquete de iconos instalado!";
        overwrite_import_string = "¡Atención! Los ajustes existentes serán sobrescritos";
        import_btn_string = "Importar";
        btn_bottomzero_string = "Espacio inferior de relleno o cuando horizontal";
        msg_success = "Éxito!";
        modal_alarm_clock_text = "Mostrar alarma";
        next_day_string = "Mañana";
        warn_sort_cat_string = "De ahora en adelante las apps no seran reorganizadas automáticamente";
        font_string = "Tipo de letra";
        font_select_string = "Seleccione el tipo de letra:";
        orient_text = "Orientación";
        orient_auto = "Automático";
        orient_horizontal = "Horizontal";
        orient_vertical = "Vertical";
        expand_notification_text = "Gesto para expandir la barra de estado";
        widget_text = "Widgets";
        widget_background_text = "El fondo como el Appdrawer";
        widget_scroll_text = "Desplazando el widget";
        widget_enabled = "Activado";
        widget_disabled = "Discapacitados";
        info_text = "información";
        widget_up = "Alto";
        widget_down = "Abajo";
        widget_add = "Añade";
        widget_add_button = "Añadir un nuevo widget";
        widget_info = "Pulsación larga para editar la altura del widget";
        all_widget_headers_text = "Widgets activos:";
        enable_widget_header_text = "Habilitar widgets";
        widget_inform_textl = "Para acceder a los widgets, pasa el dedo a la izquierda de la pantalla de inicio.";
        widget_inform_textr = "Para acceder a los widgets, pasa directamente a la pantalla de inicio.";
        help_1 = "Presione el botón largo de la aplicación para personalizar.\nMenú para pasar a las categorías.";
        help_2 = "Pase a la derecha para llegar a la pantalla de inicio.";
        help_3 = "Pase el dedo derecho para llegar al widgets.\nSe puede desactivar en los ajustes.";
        animation_text = "Animación";
        export_error = "Incapaz de crear una exportación!";
        import_error = "Incapaz de crear una importación!";
		more_icons = "Más";
		header_part_display_text = "Presentación";
		header_part_behave_text = "Comportamiento";
		header_part_text_text = "Fuente";
		header_part_icons_text = "Iconos";

        if(default_cats == 1)
        {
            l_cat_array = [];
            l_cat_array = [
                ['icons/1.svg', "Chat"],
                ['icons/2.svg', "Internet"],
                ['icons/3.svg', "Media"],
                ['icons/4.svg', "Juegos"],
                ['icons/5.svg', "Herramientas"],
                ['icons/6.svg', "Opciones"]
            ];
        }
    }
	else if (lang == "cz")
    {
		// Czech by Tadeáš Erban
		info_text1 = "Vytvořeno Lukas Sökefeld";
        info_text2 = "Verze:";
        info_text3 = "Více informací:";
        hidden_apps_text = "Skryté aplikace";
        settings_navbar_text = "Průhledná navigační lišta";
        settings_statusbar_text = "Zobrazit stavový řádek";
        settings_update_app_list_text = "Aktualizovat list aplikací";
        app_list_updated = "List aplikací aktualizován!";
        removehomescreen_string = "Odstranit z domovské obrazovky";
        result_string = "Výsledky";
        move_apps_text = "Přesunout aplikace";
        settings_error_string = "Chyba! - Zkontrolujte nastavení!";
        settings_text_date_app = "Vybrat aplikaci pro datum";
        settings_text_clock_app = "Vybrat aplikaci pro čas";
        search_place_holder = "Vyhledat...";
        app_order_li = "Seřadit A-Z";
        app_order_us_li = "Seřadit používání";
        app_move_li = "Přesunout aplikace";
        hide_app_li = "Zobrazit skryté aplikace";
        settings_li = "Nastavení";
        rename_app = "Přejmenovat";
        start_app = "Spustit";
        add_app_to_homescreen_text = "Přidat na domovskou obrazovku";
        rm_app_from_homescreen_text = "Odstranit z domovské obrazovky";
        hide_app_text = "Skrýt";
        show_app_text = "Zobrazit";
        app_unin = "Odinstalovat";
        settings_header = "Nastavení";
        settings_homescreen_icons_text = "Ikony na domovské obrazovce";
        settings_appdrawer_text = "Typ šuplíku aplikací";
        settings_icon_type_text = "Černobílé ikony v šuplíku aplikací";
        settings_icon_type_homescreen_text = "Černobílé ikony na domovské obrazovce";
        settings_homescreen_day_time_text = "Datum a čas na domovské obrazovce";
        settings_text_color_text = "Barva textu v šuplíku aplikací";
        settings_text_colorhome_text = "Barva textu na domovské obrazovce";
        settings_read_text = "Průhlednost šuplíku aplikací";
        modal_header_text_color_text = "Barva textu v šuplíku aplikací";
        modal_header_homescreentext_color_text = "Barva textu na domovské obrazovce";
        ok_t_color_text = "Použít";
        ok_th_color_text = "Použít";
        modal_header_apd_text = "Šuplík aplikací:";
        modal_header_ormode_text = "Vybrat orientaci";
        modal_ormode_l_text = "vlevo";
        modal_ormode_r_text = "vpravo";
        modal_header_homescreen_icon_text = "Ikony na domovské obrazovce";
        modal_homescreen_icon_r_text = "Kostka";
        modal_homescreen_icon_k_text = "Žádný";
        modal_header_clock_pos_text = "Pozice hodin";
        btn_applay_clock_pos_text = "Použít";
        modal_header_clock_settings_text = "Nastavení hodin";
        modal_clock_show_text = "Obrazovka";
        modal_sec_show_text = "Zobrazit sekundy";
        modal_hour_show_text = "Vykreslit hodiny tučným písmem";
        modal_date_show_text = "Zobrazit datum";
        clock_showing_true = "Je zobrazeno";
        clock_showing_false = "Není zobrazeno";
        bg_color_text = "Barva";
        bg_img_text = "Obrázek";
        modal_header_lang_text = "Jazyk";
        modal_header_lang_text2 = "Vybrat jazyk";
        modal_lang_ger_text = "Němčina";
        modal_lang_eng_text = "Angličtina";
        modal_lang_spa_text = "Španělština";
		modal_lang_cz_text = "Čeština";
        modal_lang_it_text = "Italský";
        modal_lang_fr_text = "Francouzština";
        current_lang = "Čeština";
        settings_hs_sp_text = "Počet řádků na domovské obrazovce";
        settings_hs_sp_text_header = "Vybrat počet řádků na domovské obrazovce";
        font_size_text = "Velikost písma";
        font_cat_text = "Název kategorie";
        font_app_text = "Jména aplikací";
        font_homescreen_text = "Hodiny na domovské obrazovce";
        text_navbar_complete_transparent = "Hotovo";
        header_navbar_text = "Průhlednost navigační lišty";
        text_navbar_none_transparent = "Žádná";
        text_pad_top_input = "Horní odsazení";
        settings_homescreeen_notification_text = "Oznámení na domovské obrazovce";
        text_pad_bottom_input = "Tlačítko odsazení";
        text_settings_app_drawer = "Nastavení šuplíku aplikací";
        text_app_drawer_hori = "Sloupce (Při horizontálním otočení)";
        text_app_drawer_verti = "Sloupce (Při vertikálním otočení)";
        text_homescreensettings_drawer = "Nastavení domovské obrazovky";
        text_homescreen_zeilen = "Řádky";
        text_homescreen_spalten = "Sloupce";
        cat_settings_text = "Kategorie aplikací";
        reset_usage_text = "Resetujte používání aplikace";
        cat_default_text = "Výchozí";
        cat_custom_text = "Vlastní";
        cat_icon_size_text = "Velikost ikon";
        header_icon_source_text = "Zdroj ikon";
        btn_orginal_icon_text = "Originální";
        btn_gallery_icon_text = "Z galerie";
        btn_symbol_icon_text = "Z balíčku symbolů";
        header_select_icon_pack = "Vybrat balíček ikon";
        header_select_icon_from_iconpack_text = "Vybrat ikonu";
        header_iconpack_choose_text = "Vybrat balíček ikon";
        default_string = "Výchozí";
        progress_info_string = "Použít...";
        extra_text_string = "Další";
        export_string = "Exportovat nastavení";
        import_string = "Importovat nastavení";
        add_new_catbtn_string = "Přidat novou kategorii";
        resortapplist_string = "Přetřídit seznam aplikací";
        ask_resport_string = "Upozornění! Seřazení aplikací bylo vyhozeno!";
        resort_btn_string = "Seřadit";
        abort_btn_string = "Zrušit";
        resort_error_string = "Přetřídění funguje pouze s výchozími kategoriemi";
        export_msg_string = "Nastavení jsou exportována do standartního úložiště";
        appdrawer_align_string = "Zarovnání";
        aling_left_string = "Vlevo";
        aling_right_string = "Vpravo";
        swipe_header_string = "Směr posunu";
        settings_appdrawericonsize_string = "Velikost ikon";
        sort_apps_new_string = "Aplikace jsou nově tříděny";
        modal_border_show_string = "Pozadí";
        del_cat_text = "Odstranit";
        notifi_allow_string = "Povolte LukeLauncheru aby přijímal oznámení";
        notifi_allow_ok_string = "OK";
        no_iconpack_installed_string = "Není nainstalovaný žádný balíček ikon!";
        overwrite_import_string = "Upozornění! Existující nastavení budou přepsána!";
        import_btn_string = "Importovat";
        btn_bottomzero_string = "Spodní odsazení na 0 při horizontálním otočení";
        msg_success = "Úspěch!";
        modal_alarm_clock_text = "Zobrazit budík";
        next_day_string = "Zítra";
        warn_sort_cat_string = "Od teď aplikace nebudou tříděny automaticky!";
        font_string = "Písmo";
        font_select_string = "Vybrat písmo:";
        orient_text = "Otočení";
        orient_auto = "Automatické";
        orient_horizontal = "Horizontalní";
        orient_vertical = "Vertikální";
        expand_notification_text = "Gesto pro rozvinutí stavového řádku";
        widget_text = "Widgety";
        widget_background_text = "Pozadí jako v šuplíku aplikací";
        widget_scroll_text = "Skrolování ve Widgetu";
        widget_enabled = "Aktivní";
        widget_disabled = "Deaktivovaný";
        info_text = "Informace";
        widget_up = "Nahoru";
        widget_down = "Dolů";
        widget_add = "Přidat widget";
        widget_add_button = "Přidat nový widget";
        widget_info = "Dlouhým stisknutím upravíte výšku widgetu";
        all_widget_headers_text = "Aktivní widgety:";
        enable_widget_header_text = "Povolit widgety";
        widget_inform_textl = "Pro přístup k widgetům, posuňte doleva na domovské obrazovce.";
        widget_inform_textr = "Pro přístup k widgetům, posuňte doleva na domovské obrazovce.";
        help_1 = "Podržte prst na aplikaci pro přizpůsobení.\nOtevřete menu aby jste mohli aplikaci přesouvat mezi kategoriemi.";
        help_2 = "Posuňte doprava aby jste se dostali k domovské obrazovce.";
        help_3 = "Posuňte doprava aby jste se dostali k widgetům.\n(Může být zakázáno v nastavení)";
        animation_text = "Animace";
        export_error = "Nemůžu vytvořit export!";
        import_error = "Nemůžu importovat!";
		more_icons = "Více";
		header_part_display_text = "Zpodobení";
		header_part_behave_text = "Chování";
		header_part_text_text = "Písmo";
		header_part_icons_text = "Ikony";

        if(default_cats == 1)
        {
            l_cat_array = [];
            l_cat_array = [
                ['icons/1.svg', "Komunikace"],
                ['icons/2.svg', "Internet"],
                ['icons/3.svg', "Multimédia"],
                ['icons/4.svg', "Hry"],
                ['icons/5.svg', "Nástroje"],
                ['icons/6.svg', "Nastavení"]
            ];
        }
    }
    else if (lang == "it")
    {
        info_text1 = "Creato da Lukas Sökefeld";
        info_text2 = "Versione:";
        info_text3 = "Più informazioni:";
        hidden_apps_text = "App nascoste";
        settings_navbar_text = "Barra di navigazione trasparente";
        settings_statusbar_text = "Mostra barra di stato";
        settings_update_app_list_text = "Aggiornamento lista app";
        app_list_updated = "List app aggioranta!";
        removehomescreen_string = "Rimuovi dalla home";
        result_string = "Risultati";
        move_apps_text = "Più app";
        settings_error_string = "Errore! - Controlla le impostazioni!";
        settings_text_date_app = "Seleziona app data";
        settings_text_clock_app = "Seleziona app orologio";
        search_place_holder = "Cerca...";
        app_order_li = "Ordina A-Z";
        app_order_us_li = "Ordina uso";
        app_move_li = "Muovi app";
        hide_app_li = "Mostra app nascoste";
        settings_li = "Impostazioni";
        rename_app = "Rinomina";
        start_app = "Avvia";
        add_app_to_homescreen_text = "Aggiungi alla home";
        rm_app_from_homescreen_text = "Rimuovi dalla home";
        hide_app_text = "Nascondi";
        show_app_text = "Mostra";
        app_unin = "Disinstalla";
        settings_header = "Impostazioni";
        settings_homescreen_icons_text = "Icone sfondo";
        settings_appdrawer_text = "Tipo App-Drawer";
        settings_icon_type_text = "Icone app nero/bianco";
        settings_icon_type_homescreen_text = "Icone app nero/bianco";
        settings_homescreen_day_time_text = "Ora/Data";
        settings_text_color_text = "Colore del testo";
        settings_text_colorhome_text = "Colore del testo";
        settings_read_text = "Leggibilità";
        modal_header_text_color_text = "Colore del testo";
        modal_header_homescreentext_color_text = "Colore del testo";
        ok_t_color_text = "Applica";
        ok_th_color_text = "Applica";
        modal_header_apd_text = "App-Drawer:";
        modal_header_ormode_text = "Scegli orientamento";
        modal_ormode_l_text = "sinistra";
        modal_ormode_r_text = "destra";
        modal_header_homescreen_icon_text = "Icone sfondo";
        modal_homescreen_icon_r_text = "Cubo";
        modal_homescreen_icon_k_text = "Nessuna";
        modal_header_clock_pos_text = "Posizione dell'orologio";
        btn_applay_clock_pos_text = "Applica";
        modal_header_clock_settings_text = "Impostazioni dell'orologio";
        modal_clock_show_text = "Mostra";
        modal_sec_show_text = "Mostra secondi";
        modal_hour_show_text = "Mostra ore in grassetto";
        modal_date_show_text = "Mostra data";
        clock_showing_true = "È mostrato";
        clock_showing_false = "Non è mostrato";
        bg_color_text = "Colore";
        bg_img_text = "Immagine";
        modal_header_lang_text = "Lingua";
        modal_header_lang_text2 = "Seleziona una lingua";
        modal_lang_ger_text = "Tedesco";
        modal_lang_eng_text = "Inglese";
        modal_lang_spa_text = "Spagnolo";
        modal_lang_cz_text = "Ceco";
        modal_lang_it_text = "Italiano";
        modal_lang_fr_text = "Francese";
        current_lang = "Italiano";
        settings_hs_sp_text = "Colonne della home";
        settings_hs_sp_text_header = "Seleziona colonne della home";
        font_size_text = "Dimensione del testo";
        font_cat_text = "Titolo della categoria";
        font_app_text = "Nomi delle app";
        font_homescreen_text = "Dimensioni del testo dell'orologio";
        text_navbar_complete_transparent = "Completo";
        header_navbar_text = "Trasparenza della barra di navigazione";
        text_navbar_none_transparent = "Nessuno";
        text_pad_top_input = "Spazio dall'alto";
        settings_homescreeen_notification_text = "Notifiche";
        text_pad_bottom_input = "Bottone spazio";
        text_settings_app_drawer = "Impostazioni App-drawer";
        text_app_drawer_hori = "Colonne (Da orizzontale)";
        text_app_drawer_verti = "Colonne (Da verticale)";
        text_homescreensettings_drawer = "Impostazioni Home";
        text_homescreen_zeilen = "Righe";
        text_homescreen_spalten = "Colonne";
        cat_settings_text = "Categorie App";
        reset_usage_text = "Ripristina App utilizzo";
        cat_default_text = "Default";
        cat_custom_text = "Personalizza";
        cat_icon_size_text = "Dimensione icone";
        header_icon_source_text = "Fonte icone";
        btn_orginal_icon_text = "Orginale";
        btn_gallery_icon_text = "Dalla galleria";
        btn_symbol_icon_text = "Da un pacchetto di simboli";
        header_select_icon_pack = "Seleziona pachetto";
        header_select_icon_from_iconpack_text = "Seleziona icona";
        header_iconpack_choose_text = "Seleziona pack di icone";
        default_string = "Default";
        progress_info_string = "Applica...";
        extra_text_string = "Altro";
        export_string = "Esporta impostazioni";
        import_string = "Importa impostazioni";
        add_new_catbtn_string = "Aggiungi una categoria";
        resortapplist_string = "Riordina la lista delle app";
        ask_resport_string = "Attenzione! L'ordine delle app è stato scartato!";
        resort_btn_string = "Ordina";
        abort_btn_string = "Cancella";
        resort_error_string = "Il riordinamento funziona solo con le categorie di default";
        export_msg_string = "Le impostazioni vengono esportate nello storage standard";
        appdrawer_align_string = "Allineamento";
        aling_left_string = "Sinistra";
        aling_right_string = "Destra";
        swipe_header_string = "Direzione dello swipe";
        settings_appdrawericonsize_string = "Dimensione delle icone";
        sort_apps_new_string = "Le app saranno ordinate nuovamente per categoria";
        modal_border_show_string = "Sfondo";
        del_cat_text = "Elimina";
        notifi_allow_string = "Permetti a LukeLauncher per ricevere le notifiche";
        notifi_allow_ok_string = "Ok";
        no_iconpack_installed_string = "Nessun pack di icone è installato!";
        overwrite_import_string = "Attenzione! Le impostazioni esistenti saranno sovrascritte!";
        import_btn_string = "Importa";
        btn_bottomzero_string = "Spazio dal basso 0 quando orizzontale";
        msg_success = "Successo!";
        modal_alarm_clock_text = "Mostra allarme";
        next_day_string = "Domani";
        warn_sort_cat_string = "Da ora in poi le app non saranno ordinate automaticamente!";
        font_string = "Font";
        font_select_string = "Seleziona font:";
        orient_text = "Orientamento";
        orient_auto = "Automatico";
        orient_horizontal = "Orizzontale";
        orient_vertical = "Verticale";
        expand_notification_text = "Gesto per espandere la barra di stato";
        widget_text = "Widgets";
        widget_background_text = "Appdrawer come lo sfondo";
        widget_scroll_text = "Scorrimento nel Widget";
        widget_enabled = "Attivo";
        widget_disabled = "Disattivato";
        info_text = "Info";
        widget_up = "Su";
        widget_down = "Giù";
        widget_add = "Aggiungi";
        widget_add_button = "Aggiungi un widget";
        widget_info = "Tieni premuto per modificare l'altezza di un widget";
        all_widget_headers_text = "Widget attivi:";
        enable_widget_header_text = "Abilita widget";
        widget_inform_textl = "Per accedere ai widget scorri a sinistra nella home.";
        widget_inform_textr = "Per accedere ai widget scorri a destra nella home.";
        help_1 = "Tieni premuta una app per modificare.\nMenù da spostare nelle categorie.";
        help_2 = "Scorri a destra per arrivare alla home.";
        help_3 = "Scorri a destra per arrivare ai widget.\nPuò essere disabilitato nelle impostazioni.";
        animation_text = "Animazioni";
        export_error = "Esportazione fallita!";
        import_error = "Importazione fallita!";
        more_icons = "Altro";
        header_part_display_text = "Presentazione";
        header_part_behave_text = "Comportamento";
        header_part_text_text = "Font";
        header_part_icons_text = "Icone";

        if(default_cats == 1)
        {
            l_cat_array = [];
            l_cat_array = [
                ['icons/1.svg', "Chat"],
                ['icons/2.svg', "Internet"],
                ['icons/3.svg', "Media"],
                ['icons/4.svg', "Giochi"],
                ['icons/5.svg', "Strumenti"],
                ['icons/6.svg', "Impostazioni"]
            ];
        }
    }
    else if (lang == "fr")
    {
        info_text1 = "Créé par Lukas Sökefeld";
        info_text2 = "Version:";
        info_text3 = "Plus d'information:";
        hidden_apps_text = "Applis cachées";
        settings_navbar_text = "Barre de Navigation transparente";
        settings_statusbar_text = "Afficher la barre d'état";
        settings_update_app_list_text = "Mettre à jour la liste d'applis";
        app_list_updated = "Liste d'applis mise à jour!";
        removehomescreen_string = "Retirer de l'écran d'accueil";
        result_string = "Résultats";
        move_apps_text = "Transférer l'appli";
        settings_error_string = "Erreur! - Verifier les paramètres!";
        settings_text_date_app = "Choisir appli date";
        settings_text_clock_app = "Choisir appli horloge";
        search_place_holder = "Chercher...";
        app_order_li = "Afficher de A-Z";
        app_order_us_li = "Utilisation du tri";
        app_move_li = "Transférer l'appli";
        hide_app_li = "Afficher applis cachées";
        settings_li = "Réglages";
        rename_app = "Renommer";
        start_app = "Démarrer";
        add_app_to_homescreen_text = "Ajouter à l'écran d'accueil";
        rm_app_from_homescreen_text = "Retirer de l'écran d'accueil";
        hide_app_text = "Masquer";
        show_app_text = "Montrer";
        app_unin = "Désinstaller";
        settings_header = "Réglages";
        settings_homescreen_icons_text = "Icones d'arrière plan";
        settings_appdrawer_text = "Tirroir d'applications";
        settings_icon_type_text = "Icones d'applis Noir & Blanc";
        settings_icon_type_homescreen_text = "Icones d'applis Noir & Blanc";
        settings_homescreen_day_time_text = "Horloge/Date";
        settings_text_color_text = "Couleur du texte";
        settings_text_colorhome_text = "Couleur du texte";
        settings_read_text = "Transparence";
        modal_header_text_color_text = "Couleur du texte";
        modal_header_homescreentext_color_text = "Couleur du texte";
        ok_t_color_text = "Appliquer";
        ok_th_color_text = "Appliquer";
        modal_header_apd_text = "Tirroir d'applications:";
        modal_header_ormode_text = "Choix de l'orientation";
        modal_ormode_l_text = "Gauche";
        modal_ormode_r_text = "Droite";
        modal_header_homescreen_icon_text = "Icones d'arrière plan";
        modal_homescreen_icon_r_text = "Cube";
        modal_homescreen_icon_k_text = "Aucun";
        modal_header_clock_pos_text = "Position de l'horloge";
        btn_applay_clock_pos_text = "Appliquer";
        modal_header_clock_settings_text = "Réglage de l'horloge";
        modal_clock_show_text = "Affichage";
        modal_sec_show_text = "Montrer les secondes";
        modal_hour_show_text = "Montrer les heures en caractères gras";
        modal_date_show_text = "Montrer la date";
        clock_showing_true = "est affiché";
        clock_showing_false = "n'est pas affiché";
        bg_color_text = "Couleur";
        bg_img_text = "Image";
        modal_header_lang_text = "Langue";
        modal_header_lang_text2 = "Choisir langue";
        modal_lang_ger_text = "Allemand";
        modal_lang_eng_text = "Anglais";
        modal_lang_spa_text = "Espagnol";
        modal_lang_cz_text = "Tchèque";
        modal_lang_it_text = "Italien";
        modal_lang_fr_text = "Français";
        current_lang = "Français";
        settings_hs_sp_text = "Colonnes de l'écran d'accueil";
        settings_hs_sp_text_header = "Choisir colonnes de l'écran d'accueil";
        font_size_text = "Taille de la police";
        font_cat_text = "Titre de la catégorie";
        font_app_text = "Nom de l'appli";
        font_homescreen_text = "Taille de police de l'horloge";
        text_navbar_complete_transparent = "Complète";
        header_navbar_text = "Transparence de la barre de navigation";
        text_navbar_none_transparent = "Aucune";
        text_pad_top_input = "Clavier en haut?";
        settings_homescreeen_notification_text = "Notifications";
        text_pad_bottom_input = "Clavier en bas?";
        text_settings_app_drawer = "Réglages tirroir d'applications";
        text_app_drawer_hori = "Colonnes (si horizontale)";
        text_app_drawer_verti = "Colonnes (si verticale)";
        text_homescreensettings_drawer = "Réglages de l'écran d'accueil";
        text_homescreen_zeilen = "Rangées";
        text_homescreen_spalten = "Colonnes";
        cat_settings_text = "Categories d'application";
        reset_usage_text = "Réinitialiser l'utilisation de l'app";
        cat_default_text = "Par default";
        cat_custom_text = "Usage";
        cat_icon_size_text = "Taille d'icones";
        header_icon_source_text = "Source d'icones";
        btn_orginal_icon_text = "Originale";
        btn_gallery_icon_text = "de la Galerie";
        btn_symbol_icon_text = "du pack de symboles";
        header_select_icon_pack = "Choisir pack d'icones";
        header_select_icon_from_iconpack_text = "Choisir une icone";
        header_iconpack_choose_text = "Choisir pack d'icones";
        default_string = "Par défault";
        progress_info_string = "Appliquer...";
        extra_text_string = "Supplémentaire";
        export_string = "Exporter les réglages";
        import_string = "Importer les réglages";
        add_new_catbtn_string = "Ajouter une catégorie";
        resortapplist_string = "Trier liste d'applis";
        ask_resport_string = "Attention! Tri de liste abandonné!";
        resort_btn_string = "Trier";
        abort_btn_string = "Arrêter";
        resort_error_string = "Tri catégories par défault seulement";
        export_msg_string = "Réglages exportés dans stockage principal";
        appdrawer_align_string = "Alignement";
        aling_left_string = "Gauche";
        aling_right_string = "Droite";
        swipe_header_string = "Balayer vers une direction";
        settings_appdrawericonsize_string = "Taille d'icone";
        sort_apps_new_string = "Applis récentes triées seulement";
        modal_border_show_string = "Arrière plan";
        del_cat_text = "Effacer";
        notifi_allow_string = "Autoriser LukeLauncher à recevoir des notifications";
        notifi_allow_ok_string = "Ok";
        no_iconpack_installed_string = "Pas de pack d'incoes installé!";
        overwrite_import_string = "Attention! Données réglages existants seront écrasés!";
        import_btn_string = "Importer";
        btn_bottomzero_string = "clavier en bas 0 quand horizontal";
        msg_success = "Réussi!";
        modal_alarm_clock_text = "Montrer l'alarme";
        next_day_string = "Demain";
        warn_sort_cat_string = "From now on Apps won't be sorted automatically!";
        font_string = "Police d'écriture";
        font_select_string = "Selectionner police d'écriture:";
        orient_text = "Orientation";
        orient_auto = "Automatique";
        orient_horizontal = "Horizontale";
        orient_vertical = "Verticale";
        expand_notification_text = "Geste pour étendre la barre d'état";
        widget_text = "Widgets";
        widget_background_text = "Couleur du Tirroir d'applis identique à l'arrière plan";
        widget_scroll_text = "Défilement dans Widget";
        widget_enabled = "Activer";
        widget_disabled = "Désactiver";
        info_text = "Info";
        widget_up = "Haut";
        widget_down = "Bas";
        widget_add = "Ajouter";
        widget_add_button = "Ajouter nouveau widget";
        widget_info = "Appui long pour modifier la hauteur du widget";
        all_widget_headers_text = "Activer les widgets:";
        enable_widget_header_text = "Autoriser les widgets";
        widget_inform_textl = "Accès Widgets, glisser à gauche sur l'écran d'accueil";
        widget_inform_textr = "Accès Widgets, glisser à droite sur l'écran d'accueil";
        help_1 = "Appuis long pour personnaliser.\nMenu pour aller aux Catégories";
        help_2 = "Glisser vers la droite pour revenir à l'écran d'accueil.";
        help_3 = "Glisser vers la droite pour revenir aux widgets.\nPeut être désactiver dans les réglages";
        animation_text = "Animations";
        export_error = "Impossible d'exporter!";
        import_error = "Impossible d'importer!";
        more_icons = "Plus";
        header_part_display_text = "Presentation";
        header_part_behave_text = "Procéder";
        header_part_text_text = "Police";
        header_part_icons_text = "Icones";

        if(default_cats == 1)
        {
            l_cat_array = [];
            l_cat_array = [
                ['icons/1.svg', "Chat"],
                ['icons/2.svg', "Internet"],
                ['icons/3.svg', "Media"],
                ['icons/4.svg', "Games"],
                ['icons/5.svg', "Tools"],
                ['icons/6.svg', "Settings"]
            ];
        }
    }
    else //Last alternative Englisch
    {
        info_text1 = "Created by Lukas Sökefeld";
        info_text2 = "Version:";
        info_text3 = "More information:";
        hidden_apps_text = "Hidden apps";
        settings_navbar_text = "Transparent navigation bar";
        settings_statusbar_text = "Show status bar";
        settings_update_app_list_text = "Update app list";
        app_list_updated = "App list updated!";
        removehomescreen_string = "Remove from homescreen";
        result_string = "Results";
        move_apps_text = "Move apps";
        settings_error_string = "Error! - check settings!";
        settings_text_date_app = "Select date app";
        settings_text_clock_app = "Select clock app";
        search_place_holder = "Search...";
        app_order_li = "Sort A-Z";
        app_order_us_li = "Sort usage";
        app_move_li = "Move apps";
        hide_app_li = "Show hidden apps";
        settings_li = "Settings";
        rename_app = "Rename";
        start_app = "Start";
        add_app_to_homescreen_text = "Add to homescreen";
        rm_app_from_homescreen_text = "Remove from homescreen";
        hide_app_text = "Hide";
        show_app_text = "Show";
        app_unin = "Uninstall";
        settings_header = "Settings";
        settings_homescreen_icons_text = "Background icons";
        settings_appdrawer_text = "App-Drawer type";
        settings_icon_type_text = "App icons black/white";
        settings_icon_type_homescreen_text = "App icons black/white";
        settings_homescreen_day_time_text = "Time/date";
        settings_text_color_text = "Text color";
        settings_text_colorhome_text = "Text color";
        settings_read_text = "Readability";
        modal_header_text_color_text = "Text color";
        modal_header_homescreentext_color_text = "Text color";
        ok_t_color_text = "Applay";
        ok_th_color_text = "Applay";
        modal_header_apd_text = "App-Drawer:";
        modal_header_ormode_text = "Choose orientation";
        modal_ormode_l_text = "left";
        modal_ormode_r_text = "right";
        modal_header_homescreen_icon_text = "Background icons";
        modal_homescreen_icon_r_text = "Cube";
        modal_homescreen_icon_k_text = "None";
        modal_header_clock_pos_text = "Clock position";
        btn_applay_clock_pos_text = "Applay";
        modal_header_clock_settings_text = "Clock setting";
        modal_clock_show_text = "Display";
        modal_sec_show_text = "Show seconds";
        modal_hour_show_text = "Show bold hours";
        modal_date_show_text = "Show date";
        clock_showing_true = "Is displayed";
        clock_showing_false = "Is not displayed";
        bg_color_text = "Color";
        bg_img_text = "Image";
        modal_header_lang_text = "Language";
        modal_header_lang_text2 = "Select language";
        modal_lang_ger_text = "German";
        modal_lang_eng_text = "English";
        modal_lang_spa_text = "Spanish";
        modal_lang_cz_text = "Czech";
        modal_lang_it_text = "Italian";
        modal_lang_fr_text = "French";
        current_lang = "English";
        settings_hs_sp_text = "Homescreen columns";
        settings_hs_sp_text_header = "Select homescreen columns";
        font_size_text = "Fontsize";
        font_cat_text = "Category title";
        font_app_text = "App Names";
        font_homescreen_text = "Fontsize clock";
        text_navbar_complete_transparent = "Complete";
        header_navbar_text = "Navigation bar transparency";
        text_navbar_none_transparent = "None";
        text_pad_top_input = "Padding top";
        settings_homescreeen_notification_text = "Notifications";
        text_pad_bottom_input = "Padding button";
        text_settings_app_drawer = "App-drawer settings";
        text_app_drawer_hori = "Columns (When horizontal)";
        text_app_drawer_verti = "Columns (When vertical)";
        text_homescreensettings_drawer = "Homescreen settings";
        text_homescreen_zeilen = "Rows";
        text_homescreen_spalten = "Columns";
        cat_settings_text = "App categories";
        reset_usage_text = "Reset app usage";
        cat_default_text = "Default";
        cat_custom_text = "Custom";
        cat_icon_size_text = "Icon size";
        header_icon_source_text = "Icon source";
        btn_orginal_icon_text = "Orginal";
        btn_gallery_icon_text = "From gallery";
        btn_symbol_icon_text = "From symbol pack";
        header_select_icon_pack = "Select pack";
        header_select_icon_from_iconpack_text = "Select icon";
        header_iconpack_choose_text = "Select icon pack";
        default_string = "Default";
        progress_info_string = "Apply...";
        extra_text_string = "Additional";
        export_string = "Export settings";
        import_string = "Import settings";
        add_new_catbtn_string = "Add a new cat";
        resortapplist_string = "Resort app list";
        ask_resport_string = "Attention! The app sorting is discarded!";
        resort_btn_string = "Sort";
        abort_btn_string = "Abort";
        resort_error_string = "Resort only works with default categories";
        export_msg_string = "Settings are exported to standard storage";
        appdrawer_align_string = "Alignment";
        aling_left_string = "Left";
        aling_right_string = "Right";
        swipe_header_string = "Swipe direction";
        settings_appdrawericonsize_string = "Icon size";
        sort_apps_new_string = "Apps are sorted new";
        modal_border_show_string = "Background";
        del_cat_text = "Delete";
        notifi_allow_string = "Allow LukeLauncher in order to recive notifications";
        notifi_allow_ok_string = "Ok";
        no_iconpack_installed_string = "No icon pack installed!";
        overwrite_import_string = "Attention! The existing settings will be overwritten!";
        import_btn_string = "Import";
        btn_bottomzero_string = "Padding bottom 0 when horizontal";
        msg_success = "Success!";
        modal_alarm_clock_text = "Show alarm";
        next_day_string = "Tomorrow";
        warn_sort_cat_string = "From now on Apps won't be sorted automatically!";
        font_string = "Typeface";
        font_select_string = "Select typeface:";
        orient_text = "Orientation";
        orient_auto = "Automatic";
        orient_horizontal = "Horizontal";
        orient_vertical = "Vertical";
        expand_notification_text = "Gesture to expand statusbar";
        widget_text = "Widgets";
        widget_background_text = "Appdrawer like background";
        widget_scroll_text = "Scrolling in Widget";
        widget_enabled = "Active";
        widget_disabled = "Deactivated";
        info_text = "Info";
        widget_up = "Up";
        widget_down = "Down";
        widget_add = "Add";
        widget_add_button = "Add new widget";
        widget_info = "Long press to edit widget height";
        all_widget_headers_text = "Active widgets:";
        enable_widget_header_text = "Enable widgets";
        widget_inform_textl = "To access widgets swipe left on the homescreen.";
        widget_inform_textr = "To access widgets swipe right on the homescreen.";
        help_1 = "Press app long to customize.\nMenu to move to categories.";
        help_2 = "Swipe to the right to get to the homescreen.";
        help_3 = "Swipe right to get to the widgets.\nCan be disabled in the settings.";
        animation_text = "Animations";
        export_error = "Unable to create export!";
        import_error = "Unable to import!";
		more_icons = "More";
		header_part_display_text = "Presentation";
		header_part_behave_text = "Behavior";
		header_part_text_text = "Font";
		header_part_icons_text = "Icons";

        if(default_cats == 1)
        {
            l_cat_array = [];
            l_cat_array = [
                ['icons/1.svg', "Chat"],
                ['icons/2.svg', "Internet"],
                ['icons/3.svg', "Media"],
                ['icons/4.svg', "Games"],
                ['icons/5.svg', "Tools"],
                ['icons/6.svg', "Settings"]
            ];
        }
    }

    document.getElementById("modal_header_clock_settings").textContent = modal_header_clock_settings_text;
    document.getElementById("settings_homescreen").textContent = text_homescreensettings_drawer;
    document.getElementById("modal_header_homescreen_settings").textContent = text_homescreensettings_drawer;
    document.getElementById("modal_header_app_drawer_settings").textContent = text_settings_app_drawer;
    document.getElementById("settings_app_drawer").textContent = text_settings_app_drawer;
    document.getElementById("padding_bottom_input").textContent = text_pad_bottom_input;
    document.getElementById("pad_top_input").textContent = text_pad_top_input;
    document.getElementById("settings_homescreeen_notification").textContent = settings_homescreeen_notification_text;
    document.getElementById("modal_trans_0").textContent = text_navbar_none_transparent;
    document.getElementById("modal_trans_2s").textContent = text_navbar_complete_transparent;
    document.getElementById("modal_header_text_navbar").textContent = header_navbar_text;
    document.getElementById("text_font_cat").textContent = font_cat_text;
    document.getElementById("text_font_appnames").textContent = font_app_text;
    document.getElementById("text_font_homescreen").textContent = font_homescreen_text;
    document.getElementById("modal_header_fontsize").textContent = font_size_text;
    //document.getElementById("settings_font_size").textContent = font_size_text;
    document.getElementById("settings_navbar").textContent = settings_navbar_text;
    document.getElementById("settings_statusbar").textContent = settings_statusbar_text;
    document.getElementById("settings_update_app_list").textContent = settings_update_app_list_text;
    document.getElementById("removehomescreentext").textContent = removehomescreen_string;
    document.getElementById("modal_header_date").textContent = settings_text_date_app;
    document.getElementById("modal_header_clock").textContent = settings_text_clock_app;
    document.getElementById("settings_date_app").textContent = settings_text_date_app;
    document.getElementById("settings_clock_app").textContent = settings_text_clock_app;
    document.getElementById("sif").placeholder = search_place_holder;
    document.getElementById("searchappselecclock").placeholder = search_place_holder;
    document.getElementById("searchappselecdate").placeholder = search_place_holder;
    document.getElementById("app_order_li").textContent = app_order_li;
    document.getElementById("app_order_us_li").textContent = app_order_us_li;
    document.getElementById("app_move_li").textContent = app_move_li;
    document.getElementById("hide_app_li").textContent = hide_app_li;
    document.getElementById("settings_li").textContent = settings_li;
    document.getElementById("rnb").textContent = rename_app;
    document.getElementById("btn_start_app").textContent = start_app;
    document.getElementById("btn_homescreen").textContent = add_app_to_homescreen_text;
    document.getElementById("btn_app_aus").textContent = hide_app_text;
    document.getElementById("btn_app_uninstall").textContent = app_unin;
    document.getElementById("settings_text_header").textContent = settings_header;
    document.getElementById("settings_homescreen_icons").textContent = settings_homescreen_icons_text;
    document.getElementById("settings_icon_type").textContent = settings_icon_type_text;
    document.getElementById("settings_icon_type_homescreeen").textContent = settings_icon_type_homescreen_text;
    document.getElementById("settings_homescreen_day_time").textContent = settings_homescreen_day_time_text;
    document.getElementById("settings_text_color").textContent = settings_text_color_text;
    document.getElementById("settings_homescreen_color").textContent = settings_text_colorhome_text;
    document.getElementById("settings_read").textContent = settings_read_text;
    document.getElementById("modal_header_text_color").textContent = modal_header_text_color_text;
    document.getElementById("modal_header_text_colorhomescreen").textContent = modal_header_homescreentext_color_text;
    document.getElementById("modal_homescreen_icon_k").textContent = modal_homescreen_icon_k_text;
    document.getElementById("modal_homescreen_icon_r").textContent = modal_homescreen_icon_r_text;
    document.getElementById("modal_clock_show").textContent = modal_clock_show_text;
    document.getElementById("modal_sec_show").textContent = modal_sec_show_text;
    document.getElementById("modal_hour_show").textContent = modal_hour_show_text;
    document.getElementById("modal_date_show").textContent = modal_date_show_text;
    document.getElementById("modal_alarm_clock").textContent = modal_alarm_clock_text;

    //From main.js
    document.getElementById("modal_date_1").textContent = dayjs().locale('de').format('DD.MM.YYYY');
    document.getElementById("modal_date_2").textContent = dayjs().locale('de').format('DD.MM.YY');
    document.getElementById("modal_date_3").textContent = dayjs().locale('de').format('DD.MM');

    document.getElementById("modal_date_8").textContent = dayjs().locale('de').format('MM.DD.YYYY') + " (M.D)";
    document.getElementById("modal_date_9").textContent = dayjs().locale('de').format('MM.DD.YY') + " (M.D)";
    document.getElementById("modal_date_10").textContent = dayjs().locale('de').format('MM.DD') + " (M.D)";

    if(lang == "eng")
    {
        document.getElementById("modal_date_4").textContent = dayjs().locale('en').format('D. MMMM');
        document.getElementById("modal_date_5").textContent = dayjs().locale('en').format('dddd, D. MMMM');

        document.getElementById("modal_date_6").textContent = dayjs().locale('en').format('dd, DD.MM.YY');
        document.getElementById("modal_date_7").textContent = dayjs().locale('en').format('dd, DD.MM');

        document.getElementById("modal_date_11").textContent = dayjs().locale('en').format('dd, MM.DD') + " (M.D)";
        document.getElementById("modal_date_12").textContent = dayjs().locale('en').format('dd, MM.DD.YY') + " (M.D)";
    }
    else
    {
        document.getElementById("modal_date_4").textContent = dayjs().locale('de').format('D. MMMM');
        document.getElementById("modal_date_5").textContent = dayjs().locale('de').format('dddd, D. MMMM');

        document.getElementById("modal_date_6").textContent = dayjs().locale('de').format('dd, DD.MM.YY');
        document.getElementById("modal_date_7").textContent = dayjs().locale('de').format('dd, DD.MM');

        document.getElementById("modal_date_11").textContent = dayjs().locale('de').format('dd, MM.DD') + " (M.D)";
        document.getElementById("modal_date_12").textContent = dayjs().locale('de').format('dd, MM.DD.YY') + " (M.D)";
    }

    document.getElementById("settings_lang").textContent = modal_header_lang_text;
    document.getElementById("modal_header_lang").textContent = modal_header_lang_text2;
    document.getElementById("modal_lang_ger").textContent = modal_lang_ger_text;
    document.getElementById("modal_lang_eng").textContent = modal_lang_eng_text;
    document.getElementById("modal_lang_spa").textContent = modal_lang_spa_text;
    document.getElementById("modal_lang_cz").textContent = modal_lang_cz_text;
    document.getElementById("modal_lang_it").textContent = modal_lang_it_text;
    document.getElementById("modal_lang_fr").textContent = modal_lang_fr_text;
    document.getElementById("settings_current_lang").textContent = current_lang;
    document.getElementById("settings_info_text1").textContent = info_text1;
    document.getElementById("settings_info_text3").textContent = info_text3;
    document.getElementById("settings_cat").textContent = cat_settings_text;
    document.getElementById("cat_dis_0").textContent = cat_default_text;
    document.getElementById("cat_dis_1").textContent = cat_custom_text;
    document.getElementById("modal_header_catdis").textContent = cat_settings_text;
    document.getElementById("header_icon_source_dialog").textContent = header_icon_source_text;
    document.getElementById("btn_orginal_icon").textContent = btn_orginal_icon_text;
    document.getElementById("btn_gallery_icon").textContent = btn_gallery_icon_text;
    document.getElementById("btn_symbol_icon").textContent = btn_symbol_icon_text;
    document.getElementById("header_select_icon_pack").textContent = header_select_icon_pack;
    document.getElementById("header_select_icon_from_iconpack").textContent = header_select_icon_from_iconpack_text;
    document.getElementById("searchiconinpack_input").placeholder = search_place_holder;
    document.getElementById("searchiconinpack_cat_input").placeholder = search_place_holder;
    document.getElementById("settings_reset_usage").textContent = reset_usage_text;

    document.getElementById("header_icon_source_cat").textContent = header_icon_source_text;
    document.getElementById("btn_orginal_cat_icon").textContent = btn_orginal_icon_text;
    document.getElementById("btn_gallery_cat_icon").textContent = btn_gallery_icon_text;
    document.getElementById("btn_symbol_cat_icon").textContent = btn_symbol_icon_text;

    document.getElementById("header_icon_select_pack_cat").textContent = header_select_icon_pack;
    document.getElementById("header_searchiconinpack_cat").textContent = header_select_icon_from_iconpack_text;

    document.getElementById("header_iconpack_choose").textContent = header_iconpack_choose_text;

    document.getElementById("header_progress_dialog").textContent = progress_info_string;
    document.getElementById("settings_extra_header").textContent = extra_text_string;

    document.getElementById("modal_header_extra_settings").textContent = extra_text_string;


    document.getElementById("settings_export").textContent = export_string;
    document.getElementById("settings_import").textContent = import_string;

    document.getElementById("add_new_cat_button").textContent = add_new_catbtn_string;
    document.getElementById("settings_resortapplist").textContent = resortapplist_string;

    document.getElementById("modal_header_askquestion_settings").textContent = ask_resport_string;

    document.getElementById("btn_resort_start").textContent = resort_btn_string;
    document.getElementById("btn_resort_abort").textContent = abort_btn_string;

    document.getElementById("settings_appdrawer_align").textContent = appdrawer_align_string;
    document.getElementById("modal_header_align").textContent = appdrawer_align_string;

    document.getElementById("settings_appdrawer_swipe").textContent = swipe_header_string;

    document.getElementById("modal_border_show").textContent = modal_border_show_string;

    document.getElementById("modal_header_notifi_allow").textContent = notifi_allow_string;

    document.getElementById("btn_notifi_allow").textContent = notifi_allow_ok_string;

    document.getElementById("modal_header_askimport_settings").textContent = overwrite_import_string;


    document.getElementById("btn_import_start").textContent = import_btn_string;
    document.getElementById("btn_import_abort").textContent = abort_btn_string;

    document.getElementById("padding_bottom_input_set_zero").textContent = btn_bottomzero_string;

    document.getElementById("settings_text_font_appdrawer").textContent = font_string;
    document.getElementById("modal_header_text_font_appdrawer").textContent = font_select_string;

    document.getElementById("settings_text_font_homescreen").textContent = font_string;
    document.getElementById("modal_header_text_font_homescreen").textContent = font_select_string;

    document.getElementById("normal_font_appdrawer_item").textContent = default_string;
    document.getElementById("normal_font_homescreen_item").textContent = default_string;

    document.getElementById("settings_orient").textContent = orient_text;
    document.getElementById("modal_header_orient").textContent = orient_text;

    document.getElementById("modal_orient_auto").textContent = orient_auto;
    document.getElementById("modal_orient_horizontal").textContent = orient_horizontal;
    document.getElementById("modal_orient_vertical").textContent = orient_vertical;

    document.getElementById("settings_notification_expand_text").textContent = expand_notification_text;

    document.getElementById("settings_widget").textContent = widget_text;
    document.getElementById("modal_header_text_widget").textContent = widget_text;
    document.getElementById("widget_background_input_text").textContent = widget_background_text;
    document.getElementById("widget_scroll_input_text").textContent = widget_scroll_text;

    document.getElementById("settings_show_info").textContent = info_text;
    document.getElementById("settings_show_info_head").textContent = info_text;

    document.getElementById("add_new_widget_button").textContent = widget_add_button;
    document.getElementById("all_widget_headers").textContent = all_widget_headers_text;
    document.getElementById("enable_widget_header").textContent = enable_widget_header_text;

    document.getElementById("settings_animations").textContent = animation_text;

    document.getElementById("modal_header_homescreenicons").textContent = modal_header_homescreen_icon_text;

	document.getElementById("header_part_display_appdrawer").textContent = header_part_display_text;
	document.getElementById("header_part_display_homescreen").textContent = header_part_display_text;
	document.getElementById("header_part_behave_appdrawer").textContent = header_part_behave_text;
	document.getElementById("header_part_behave_homescreen").textContent = header_part_behave_text;
	document.getElementById("header_part_text_appdrawer").textContent = header_part_text_text;
	document.getElementById("header_part_text_homescreen").textContent = header_part_text_text;
	document.getElementById("header_part_icons_appdrawer").textContent = header_part_icons_text;
	document.getElementById("header_part_icons_homescreen").textContent = header_part_icons_text;

    if(default_cats == 1)
    {
        for (var i = 0; i < l_cat_array.length; i++)
        {
            cat_array[i].icon = l_cat_array[i][0];
            cat_array[i].name = l_cat_array[i][1];
        }
    }

    if(current_kat != -1)
    {
        if(current_kat > cat_array.length)
        {
            current_kat = 1;
        }
        document.getElementById("Cat_name_text").textContent = cat_array[current_kat - 1][1]; // kat_list[current_kat];
    }
}
