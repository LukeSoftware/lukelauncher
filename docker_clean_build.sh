#! /usr/bin/env bash

_SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
_BUILD_DIR="$_SCRIPT_DIR/.build"

docker build --tag lukelauncher .
#docker run --rm --user "$(id -u):$(id -g)" -v "${_SCRIPT_DIR}":"/lukelauncher" -v "${_BUILD_DIR}/root":"/root" -it lukelauncher bash -c "cd /lukelauncher;yes no | cordova build; chmod -R 777 /lukelauncher/platforms/android/app/build/outputs"
docker run --rm -v "${_SCRIPT_DIR}":"/lukelauncher" -v "${_BUILD_DIR}/root":"/root" -it lukelauncher bash -c "cd /lukelauncher;yes no | cordova build; chmod -R 777 /lukelauncher/platforms/android/app/build/outputs"
#docker run --rm -v "${_SCRIPT_DIR}":"/lukelauncher" -it lukelauncher bash
#docker run --rm -v "${_SCRIPT_DIR}":"/lukelauncher" -it beevelop/cordova bash

#docker run --rm -v "${_SCRIPT_DIR}":"/lukelauncher" -it hamdifourati/cordova-android-builder bash
