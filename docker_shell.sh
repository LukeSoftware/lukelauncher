#! /usr/bin/env bash

_SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
_BUILD_DIR="$_SCRIPT_DIR/.build"

docker build --tag lukelauncher .
docker run --rm --user "$(id -u):$(id -g)" -v "${_SCRIPT_DIR}":"/lukelauncher" -v "${_BUILD_DIR}/root":"/root" -it lukelauncher bash